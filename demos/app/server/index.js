var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var _ = require('lodash');

// Send HTML app for root
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

// Expose node_modules as static directory
app.use(express.static('node_modules'));


// Keep track of connections
var connections = new Map();

// Listen for new web-socket connections
io.on('connection', socket => {

  // Listen for 'add_node' message from client
  socket.on('add_node', data => {
    console.log(`New node: ${socket.id}: ${JSON.stringify(data)}`);
    connections.set(socket.id, _.extend({}, data, { ip: socket.request.connection.remoteAddress }));
  });

  // Listen for client disconnects
  socket.on('disconnect', function() {
    console.log(`Node ${socket.id} disconnected`);
    // Remove from list of connections
    connections.delete(socket.id);
  });
});

// Send a heartbeat every 1 second with new data
setInterval(function() {

  console.log(`${connections.size} active ${connections.size === 1 ? 'connection' : 'connections'}`);

  // Build raw HTML
  var data = _.map(Array.from(connections), ([id, data]) => {
    return `<li>${data.ip} ${data.label} (${id})</li>`;
  }).join('');

  // Emit heartbeat to all connected clients
  io.emit('heartbeat', data);

}, 1000);

// Listen on port 3000
http.listen(3000, function(){
  console.log('listening on *:3000');
});
