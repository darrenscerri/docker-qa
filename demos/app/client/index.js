// Connect to server
// Use ENV variable "SERVER_URL" as server url or fallback to localhost if ENV variable is not specified
var socket = require('socket.io-client')(process.env.SERVER_URL || 'http://localhost:3000');

// Use ENV variable "LABEL" as client label or fallback to machine hostname
var label = process.env.LABEL || require('os').hostname();

// Wait for web-socket connections
socket.on('connect', function(){
  console.log('connected');

  // Emit 'add_node' message to notify server and add client data
  socket.emit('add_node', { label });
});

// Listen for disconnects
socket.on('disconnect', function(){
  console.log('disconnected');
});
