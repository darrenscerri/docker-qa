# Container-powered QA using Docker - Darren Scerri

## Demos

### Demo 1

* `docker run -it ubuntu`
* `docker run -it ubuntu:14.04`
* `docker run -it ubuntu echo "hello"`
* `./demos/start.sh`

### Demo 2

* `cd ./demos/mutual-ping`
* `docker-compose up`

### Demo 3

* `docker create -v /data --name="data-volume" busybox`
* Write some files in the container in `/data`
* `docker run -it --volumes-from="data-volume" ubuntu`
* `/data` from the data volume is mounted in the container and written files can be read and written normally

### Demo 4

This demo involves a Node.js server-client app. The server acts as an HTTP server exposing port 3000 and also running a web-socket server. Visiting `http://localhost:3000` would show a list of active connections and adds the browser as an active connection. The Node.js client app acts similarly and connects with the server using a web-socket.

Adding a query string `?label=<something>` will also send the web server the label which is then displayed along with the active connection.

* `cd ./demos/app/server`
* `docker build -t <image-name> .`
* Server image is now built and can be run or pushed
* `docker push <image-name>`

You can run the image by running: `docker run -it <image-name>`. Expose ports to host machine by specifying the `-p` switch: `docker run -it -p 3000:3000 <image-name>`

You can also build the client image in the same way as you built the server image.

You can specify the URL that the client connects to using the environment variable `SERVER_URL` and specify a custom label using the environment variable `LABEL`.

Run as follows: `docker run -it -e SERVER_URL="http://<ip>:3000" -e LABEL="<label>" <client-image-name>`

Images are also available to pull at https://hub.docker.com/r/darrenscerri/.

Server Image: `darrenscerri/ws-server`

Client Image: `darrenscerri/ws-client`

### Demo 5

#### docker commit

Take snapshots of containers using `docker commit`. Take any container id and run `docker commit <container-id> <new-image-name>`. The image is created and can be pushed and run as a regular image.

You can save images by running `docker save > <image-name>.tar`.

Saved images can be loaded by running `docker load < <image-name>.tar`

#### Selenium Grid

* `cd ./demos/selenium`
* `docker-compose up`
* Open a separate terminal session
* `cd ./demos/selenium`
* `docker-compose scale chrome=10`
* Selenium grid is available on port 4444

____
© Darren Scerri - 2016

*contact@darrenscerri.com*
